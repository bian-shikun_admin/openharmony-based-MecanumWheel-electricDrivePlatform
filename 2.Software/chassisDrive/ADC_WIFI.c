#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "wifiiot_gpio_ex.h"
#include "wifiiot_errno.h"
#include "wifiiot_adc.h"

//wifi 调用
#include "wifi_device.h"
#include "lwip/netifapi.h"
#include "lwip/api_shell.h"
#include <netdb.h>
#include <stdlib.h>
#include "lwip/sockets.h"
#include "wifi_connect.h"
//NFC
#include "wifiiot_gpio.h"
#include "wifiiot_i2c.h"
#include "wifiiot_i2c_ex.h"
#include "nfc.h"

#define ADC_TASK_STACK_SIZE 1024 * 10
#define ADC_TASK_PRIO 24
#define _PROT_ 8888
#define data_number 100





//在sock_fd 进行监听，在 new_fd 接收新的链接
int WIFI_suceesful_flag;
int WiFi_m;
int sock_fd;
int send_len;


//定时器/
uint32_t exec1;
int time_flag=0;
int time_flag1=0;


WifiIotI2cData I2C_data = {0};





uint8_t memary_buf[16*8];
char WIFI_NAME[16*2];
char WIFI_PASSWORD[16*2];
char inter_ip[16*3];
char inter_prot[8];
int addr_length;
char  char_Data[10]={'0','1','2','3','4','5','6','7','8','9'};
char UDP_Data[2000];
char UDP_Data1[2000];

char semicolon=';';
char minus='-';
char art='@';
//创建服务器地址
struct sockaddr_in send_addr;
socklen_t addr_length = sizeof(send_addr);



//


int send_Enable=0;


/***** 获取电压值函数 *****/
static float GetVoltage(void)
{
    unsigned int ret;
    unsigned short data;
    ret = AdcRead(WIFI_IOT_ADC_CHANNEL_5, &data, WIFI_IOT_ADC_EQU_MODEL_8, WIFI_IOT_ADC_CUR_BAIS_DEFAULT, 0x00);
    return (float)data*4.8-5000;
}

static float GetVoltage1(void)
{
    unsigned int ret;
    unsigned short data;
    ret = AdcRead(WIFI_IOT_ADC_CHANNEL_0, &data, WIFI_IOT_ADC_EQU_MODEL_8, WIFI_IOT_ADC_CUR_BAIS_DEFAULT, 0x00);
    return (float)data*4.8-5000;
}

static float GetVoltage2(void)
{
    unsigned int ret;
    unsigned short data;
    ret = AdcRead(WIFI_IOT_ADC_CHANNEL_6, &data, WIFI_IOT_ADC_EQU_MODEL_8, WIFI_IOT_ADC_CUR_BAIS_DEFAULT, 0x00);
    return (float)(data-1024)/142;
}



//*********NFC端口初始化*********//
 static void NFCread_int(void)
 {  
    GpioInit();
    //GPIO_0复用为I2C1_SDA
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_0, WIFI_IOT_IO_FUNC_GPIO_0_I2C1_SDA);

    //GPIO_1复用为I2C1_SCL
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_1, WIFI_IOT_IO_FUNC_GPIO_1_I2C1_SCL);
    
    //配置LED
    //设置GPIO_2的复用功能为普通GPIO
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_2, WIFI_IOT_IO_FUNC_GPIO_2_GPIO);
    //设置GPIO_2为输出模式
    GpioSetDir(WIFI_IOT_GPIO_IDX_2, WIFI_IOT_GPIO_DIR_OUT);

    
    //开机控制
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_14, WIFI_IOT_IO_FUNC_GPIO_14_GPIO);
    //设置GPIO_14为输出模式
    GpioSetDir(WIFI_IOT_GPIO_IDX_14, WIFI_IOT_GPIO_DIR_OUT);
    GpioSetOutputVal(WIFI_IOT_GPIO_IDX_14, 1);

    //baudrate: 400kbps
    I2cInit(WIFI_IOT_I2C_IDX_1, 400000);
    I2cSetBaudrate(WIFI_IOT_I2C_IDX_1, 400000);
 }







 //创建WIFI NFC读取数据功能函数//
static void WIFI_data(void)
{
    int PIN;
    int PIN_flag=0;
    int number_flag=0;
    NT3HreadUserpages(8,memary_buf);//读取NFC标签
    printf("read:%s\r\n",memary_buf);
    for(PIN=0;PIN<140;PIN++)
    {
        if (memary_buf[PIN+5]=='@'){PIN=PIN+1;PIN_flag=1;number_flag=PIN;}
        if (memary_buf[PIN+5]=='#'){PIN=PIN+1;PIN_flag=2;number_flag=PIN;}
        if (memary_buf[PIN+5]=='%'){PIN=PIN+1;PIN_flag=3;number_flag=PIN;}
        if (memary_buf[PIN+5]=='$')break;
        switch(PIN_flag)
        {
            case (0):
            WIFI_NAME[PIN]=memary_buf[PIN+5]; 
            break;
            case (1):
            WIFI_PASSWORD[PIN-number_flag]=memary_buf[PIN+5];
            break;
            case (2):
            inter_ip[PIN-number_flag]=memary_buf[PIN+5];
            break;
            case (3):
            inter_prot[PIN-number_flag]=memary_buf[PIN+5];
            break;
            default:break;
        }
    }
         printf("NAME:%s\r\nPASS:%s\r\nIP:%s\r\nPORT:%s\r\n",WIFI_NAME,WIFI_PASSWORD,inter_ip,inter_prot);
}







//************连接WIFI************************//
static void WIFI_connect(void)
{
    for(;;)
    {    
    if(WIFI_suceesful_flag==1)break;
    WIFI_data();
    GpioSetOutputVal(WIFI_IOT_GPIO_IDX_2, 0);
    usleep(50000);
    WifiConnect(WIFI_NAME,WIFI_PASSWORD);
    GpioSetOutputVal(WIFI_IOT_GPIO_IDX_2, 1);
    usleep(500000);
    }
}
static char* char_Data_change(char * cdata,int data,int len)
{
    int number_char,number_char1,send_len1,data_minus;   
    if (data>=0)
    {
    if(data<10) 
    {
        send_len1=1;
        number_char=data;
        cdata[len]=char_Data[number_char];
    }
    if((data>=10)&&(data<100))
    {
        send_len1=2;
        number_char=data/10;
        cdata[len]=char_Data[number_char];
        number_char=data%10;
        cdata[1+len]=char_Data[number_char];
    }
    if((data>=100)&&(data<1000))
    {
        send_len1=3;
        number_char=data/100;
        cdata[len]=char_Data[number_char];
        number_char1=data/10;
        number_char=number_char1%10;
        cdata[1+len]=char_Data[number_char];
        number_char=number_char1%10;
        cdata[2+len]=char_Data[number_char];
    }
    if(data>=1000)
    {
        send_len1=4;
        number_char=data/1000;
        cdata[len]=char_Data[number_char];

        number_char1=data/100;
        number_char=number_char1%10;
        cdata[1+len]=char_Data[number_char];

        number_char1=data/10;
        number_char=number_char1%10;
        cdata[2+len]=char_Data[number_char];

        number_char=number_char1%10;
        cdata[3+len]=char_Data[number_char];
    }
    }else
    {
        data_minus=-data;
        if(data_minus<10) 
    {
        send_len1=2;        
        cdata[len]=minus;
        number_char=data_minus;
        cdata[1+len]=char_Data[number_char];
    }
    if((data_minus>=10)&&(data_minus<100))
    {
        send_len1=3;        
        cdata[len]=minus;
        number_char=data_minus/10;
        cdata[1+len]=char_Data[number_char];
        number_char=data_minus%10;
        cdata[2+len]=char_Data[number_char];
    }
    if((data_minus>=100)&&(data_minus<1000))
    {
        send_len1=4;
        cdata[len]=minus;
        number_char=data_minus/100;
        cdata[1+len]=char_Data[number_char];
        number_char1=data_minus/10;
        number_char=number_char1%10;
        cdata[2+len]=char_Data[number_char];
        number_char=number_char1%10;
        cdata[3+len]=char_Data[number_char];
    }
    if(data_minus>=1000)
    {
        send_len1=5;
        cdata[len]=minus;
        number_char=data_minus/1000;
        cdata[1+len]=char_Data[number_char];

        number_char1=data_minus/100;
        number_char=number_char1%10;
        cdata[2+len]=char_Data[number_char];

        number_char1=data_minus/10;
        number_char=number_char1%10;
        cdata[3+len]=char_Data[number_char];

        number_char=number_char1%10;
        cdata[4+len]=char_Data[number_char];
    }
    }
    send_len=send_len+send_len1;
    return  cdata;
}




//*************连接服务地址**********************************//

static void WIFI_connect_IP(void)
{   
    int i;int prot; 
    for(i=0;i<4;i++)
    { 
    if(i==0) prot=(inter_prot[i]-'0')*1000;
    if(i==1) prot=(inter_prot[i]-'0')*100+prot;
    if(i==2) prot=(inter_prot[i]-'0')*10+prot;
    if(i==3) prot=(inter_prot[i]-'0')+prot;
    }
    
    printf("prot:%d\r\n",prot);
    send_addr.sin_family = AF_INET;
    send_addr.sin_port = htons(prot);
    send_addr.sin_addr.s_addr = inet_addr(inter_ip);
    addr_length = sizeof(send_addr);
    IoSetPull(WIFI_IOT_IO_NAME_GPIO_11, WIFI_IOT_IO_PULL_UP);
}

 void LED(void)
 {
    int ddata,ddata1,ddata2;
    float voltage,voltage1,voltage2;
while(1)
{
            for(int i=0;i<data_number;i++)
        {
           voltage = GetVoltage();
           ddata=(int)voltage;
           char_Data_change(UDP_Data,ddata,send_len);      //读取ADC1的电压      
           UDP_Data[send_len]=semicolon;
           send_len=send_len+1;
        }
           UDP_Data[send_len-1]=art;

        for(int i=0;i<data_number;i++)
        {
           voltage1 = GetVoltage1();
           ddata1=(int)voltage1;
           char_Data_change(UDP_Data,ddata1,send_len);    //读取ADC2的电压        
           UDP_Data[send_len]=semicolon;
           send_len=send_len+1;
        }
           UDP_Data[send_len-1]=art;

           voltage2 = GetVoltage2();
           ddata2=(int)voltage2;
           char_Data_change(UDP_Data,ddata2,send_len);     //读取ADC3的电压
           strcpy(UDP_Data1,UDP_Data);
           memset(UDP_Data, 0, send_len);
           send_len=0;
           if(time_flag1==1)
           {
               usleep(1);
               time_flag1=0;
           }
           
    
}



 }




//*****************创建任务******************//
 static void ADCTask(void)
{
    //服务器的地址信息
    WIFI_suceesful_flag=0;
    WiFi_m=0;
    send_len=0;
    NFCread_int();
    WIFI_connect();
    WIFI_connect_IP();
    GpioSetOutputVal(WIFI_IOT_GPIO_IDX_2, 1);
    //创建socket
    if ((sock_fd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("create socket failed!\r\n");
        exit(1);
    }
    //总计发送 count 次数据
    while (1)
    {
        sendto(sock_fd,UDP_Data1,strlen(UDP_Data1), 0, (struct sockaddr *)&send_addr, addr_length);    //发送数据到服务远端
        if(time_flag==1)
           {
               usleep(1);
               time_flag=0;
           }
    }
    //关闭这个 sockets
    closesocket(sock_fd);
}


/***** 定时器1 回调函数 *****/
void Timer1_Callback(void *arg)
{
    (void)arg;
    time_flag=1;
    time_flag1=1;
}
static void ADCExampleEntry(void)
{
    osThreadAttr_t attr;
    // 定时器 //
    osTimerId_t id1;
    uint32_t timerDelay;
    osStatus_t status;
     exec1 = 1U;
    id1 = osTimerNew(Timer1_Callback, osTimerPeriodic, &exec1, NULL);
    if (id1 != NULL)
    {
        timerDelay = 1000U;
        status = osTimerStart(id1, timerDelay);
    }

    
    attr.name = "ADCTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = ADC_TASK_STACK_SIZE;
    attr.priority = ADC_TASK_PRIO;
    if (osThreadNew((osThreadFunc_t)ADCTask, NULL, &attr) == NULL)
    {
        printf("[ADCExample] Falied to create ADCTask!\n");
    }
    attr.name = "LED";
        if (osThreadNew((osThreadFunc_t)LED, NULL, &attr) == NULL)
    {
        printf("[ADCExample] Falied to create LED!\n");
    }



}

APP_FEATURE_INIT(ADCExampleEntry); 