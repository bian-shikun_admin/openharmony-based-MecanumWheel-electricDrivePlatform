# 基于openharmony的麦轮电驱平台   

## 团队成员及作品风采链接

边石坤, 岳扬, 高斌光,   
B站视频链接：https://www.bilibili.com/video/BV1NQ4y1d7Bt


## 项目应用场景介绍   

此作品的创意将用来参加openharmony成长计划学生挑战赛的活动。   
本作品将以基于openharmony系统开发的应用和设备提出解决方案，   
项目所解决的目标为：   
优质教育、体面工作和经济增长、可持续城市和建设   
目标冲击一等奖。

![数字模型](4.Dcos/pictures/chassis.png) 

## 特别说明   

本作品的设计创新还停留在模型组装及可行性试验阶段，   
不具备成为产品级项目的水准。   
但其所涉及的领域较为丰富且全面。

## 软件架构


















## 作品所涉及的关键技术及设计理念解读

1.  硬件结构：三维建模、3D打印、激光切割
2.  控制系统：openharmony南向设备开发、PCB设计
3.  上位交互：openharmony北向应用开发、arkUI前端
4.  后台处理：Particle Ability开发   

### 车身设计   

#### 主要标准件选型   

该模型的动力元件选择的是：42_0.52NM 的**步进电机**。
输出轴直径 8mm

![步进电机](4.Dcos/pictures/步进电机.jpg)  

所以为了保证与电机轴配合，**麦克纳姆轮**采用的是：外径 75mm；不带键槽联轴器；孔径8mm 的样式

![麦轮](4.Dcos/pictures/麦轮.jpg)   

为了保证步进电机和结构支撑的连接，**电机支架**在这里被选用

![电机支架](4.Dcos/pictures/电机支架.jpg) 

#### 结构支撑创意   

该支撑结构具有以下三项功能：   

 * 固定电动机驱动模块儿   
 * 对排线线束进行梳理  
 * 保证驱动模组与大梁的结合   

![结构支撑](4.Dcos/pictures/结构支撑.jpg) 



























































#### 电池支座创新   
该支座具备动力电池分接的能力：左侧的T型插口公头是输入端；右侧T型插母头是输出端，可为其他接入机构的正常工作提供电源。   
与此同时，支座上还设计了总电源开关，为系统的安全运行提供保障。   
另外，恰如其分的配合选择以及3D打印件的层叠结构，使得电池可以十分方便地实现卡入、取出。   


![电池支座](4.Dcos/pictures/电池支座.jpg) 





















### 控制系统软硬件   

* 驱动程序设计   

* 通信逻辑搭建   

这块板子用来对小熊派nano的板子接口进行转接 ~

![E66设计板](4.Dcos/pictures/PCB_E66.jpg)  

















底板上具备电源分配，信号线分接等功能

![底板设计](4.Dcos/pictures/PCB_down.jpg)   
























以上两块儿板子之间使用 2 x 6 的排线连接

![板子图](4.Dcos/pictures/板子图.jpg) 

#### 开发板引脚设计规划   

com : **V**3.3   
EN_L : IO 02 &ensp; EN_R : IO 09   
&emsp;&ensp;&ensp;&ensp;&nbsp;**FL**  &ensp;&ensp;&ensp;&nbsp;**FB**   
STP &ensp; IO 14 &nbsp;  IO 05   
DIR &ensp; IO 07 &nbsp;  IO 03   

STP &ensp; IO 13 &nbsp;  IO 06   
DIR &ensp; IO 08 &nbsp;  IO 04   
&emsp;&ensp;&ensp;&ensp;&nbsp;**BL**  &emsp;&ensp;&ensp;**BR**   
(&nbsp; 以上规划表中：F ：front；L ：left；R : right; B : back; &nbsp;)   
       
### 北向应用



## 开发设计详解

## 项目产出

● 独立设计制作装配的完整模型一部   
● 适配模型功能及空间的控制、驱动电路PCB   
● 一组交互协作的上位机程序   
● 项目设计思路和关键技术点描述文档若干   
（以贴子的形式发表在各大技术社区）   
51cto开源基础软件社区：https://ost.51cto.com/column/76   
● 开发设计历程的记录视频一套   


## 作品呈现形式

**实物模型作品** + **基于arkUI开发的且运行在openharmony开发板上的北向交互应用**
 
![实景图](4.Dcos/pictures/实景图.webp) 

## 使用说明及复现途径

1.  xxxx
2.  xxxx
3.  xxxx


## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

